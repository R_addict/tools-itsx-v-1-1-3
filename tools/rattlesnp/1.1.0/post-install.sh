#!/bin/bash

version=1.1.0

set +e

eval "$(${CONDA_HOME}/bin/conda shell.bash hook)"

conda activate rattlesnp-${version}
if [ $? -ne 0 ]
then
    conda activate ${CONDA_HOME}/envs/rattlesnp-${version}
fi

if [ 'rattlesnp-'${version} != "${CONDA_DEFAULT_ENV}" ]
then
    echo "Unable to load rattlesnp env"
    exit 1;
fi

set -e

# run install cluster command line
rattleSNP install_cluster -s slurm -e modules
