Bootstrap: docker
#From: nvidia/cuda:10.2-devel-ubuntu18.04
From: nvidia/cuda:11.7.0-devel-ubuntu18.04

%environment
  export PATH=/opt/cryodrgn/bin:$PATH

%files

%post
    CONDA=/opt/conda/bin
    apt-get update
    DEBIAN_FRONTEND=noninteractive apt -y install apt-utils apt-transport-https ca-certificates gnupg-agent software-properties-common wget git

    apt-get clean
    apt-get purge

    # install conda
    wget -q -P /tmp https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && bash /tmp/Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda && rm /tmp/Miniconda3-latest-Linux-x86_64.sh

    # Install conda packages.
    PATH="/opt/conda/bin:$PATH"
    conda update -qy conda 

    mkdir -p /opt/cryodrgn/
    cd /opt/cryodrgn

    # Create conda environment
    $CONDA/conda create --name cryodrgn1 python=3.9
    . activate cryodrgn1

    # Install dependencies
    #$CONDA/conda install pytorch==1.12.1 torchvision==0.13.1 torchaudio==0.12.1 cudatoolkit=10.2 -c pytorch
    $CONDA/conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia
    $CONDA/conda install pandas

    # Install dependencies for latent space visualization
    $CONDA/conda install seaborn scikit-learn
    $CONDA/conda install umap-learn jupyterlab ipywidgets cufflinks-py "nodejs>=15.12.0" -c conda-forge
    jupyter labextension install @jupyter-widgets/jupyterlab-manager --no-build
    jupyter labextension install jupyterlab-plotly --no-build
    jupyter labextension install plotlywidget --no-build
    jupyter lab build

    # Clone source code and install
    git clone https://github.com/zhonge/cryodrgn.git
    cd cryodrgn
    pip install .


%labels
  Author J.C. Haessig <haessigj@igbmc.fr>

%test
