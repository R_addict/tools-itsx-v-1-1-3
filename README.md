# IFB clusters tools deployment

This repository allows to manage the deployment of tools for the IFB clusters federation.

| NNCR Cluster     | IFB Contact point     | More information                                                        |
| ---------------  | --------------------- | ----------------------------------------------------------------------- |
| IFB Core Cluster | @lecorguille, @julozi | https://ifb-elixirfr.gitlab.io/cluster/doc/                             |
| ABiMS            | @lecorguille          | https://abims.sb-roscoff.fr/resources/cluster                           |
| IGBMC            | @julozi               | https://it.igbmc.fr/calcul-scientifique |
| CCUS             | @julozi               | https://hpc.pages.unistra.fr/                                           |
| BiRD             | @Jeff_MrBE4R          | https://pf-bird.univ-nantes.fr/resources/birdcluster                    |
| MCIA             | @dbenaben             | https://www.mcia.fr/                                                    |
| RPBS             | @reyjul               | https://bioserv.rpbs.univ-paris-diderot.fr/                             |
| CIRAD            | @pitollat             | https://bioinfo-agap.cirad.fr/                                          |


# Add a tool

1. Add a new tool or tool version definition (see [Tool definition format](#tool-definition-format)). There are examples of well-written definition in ["tools/sample"](https://gitlab.com/ifb-elixirfr/cluster/tools/tree/master/tools/sample) you can copy.
2. Propose the change as a merge request. Your definition will automatically be linted and deployed on a docker environment to test that it works, but the tool will not yet be available on the IFB clusters federation.
3. Once the definition is ready it will be merged and deployed automatically to the federated clusters.

**Tips:** To interact with a GitLab repository, you can use both the [web interface](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or git through [command lines](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line).

**Note:** If you aren't part of this repository developer, GitLab will [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) this repo for you on your account.

# Conda
## Quick mode

For Conda packages that come from either [`bioconda`](https://bioconda.github.io/) or [`conda-forge`](https://anaconda.org/conda-forge), you just have to add one line in one file.
You can use both the [web interface](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or git through [command lines](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line)

Example: tools/braker2/2.1.4/meta.yml
```
deployment: conda
```

Our 🤖[@ifbot](https://gitlab.com/ifbot) will process his magic to complete the [`meta.yml`](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/tools/braker2/2.1.4/meta.yml) and the [`env.yml`](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/tools/braker2/2.1.4/env.yml)
And eventually

**If:** ✅
One of the maintainers will merge your Merge Request and trigger the installation on the different production infrastructures.


**Else:** 💣you get this kind of message:
```my tools v1.1
✨ I added a conda environment definition
💣 Incomplete meta data and unable to complete them automatically
```
You may have to complete the [description](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/tools/pipmir/1.1/meta.yml#L3-4) on the same branch.

:warning: Make sure to allow the maintainers of the tools repository to push on your branch in the Merge Request settings if you want the :robot: [@ifbot](https://gitlab.com/ifbot) to complete your contribution.


## Command line script to help deploying the Conda tool

The repository include a script to let you create quickly the definition of conda deployment.

This tools requires the anaconda tool (anaconda-client) in your PATH.

```
bash add_conda_tool <channel>/<software>/<version>
```

The script will generate automatically the two files required for the deployment definition. You will just have to push them in your repository and create a merge request.

# Conda and Singularity - Tool definition format

Each tool version to deploy must be defined by :
- A `meta.yml` file that give general information on the software and how it should be deployed
- A definition file that is either a conda environment or a Singularity image definition

The `meta.yml` accepts the following fields :

## Common

| Property          | Mandatory | Description                              |
|-------------------|:---------:|------------------------------------------|
| deployment        |     X     | Type of tool (conda or singularity)      |
| about.description |     X     | One line description of the tool         |
| about.url         |           | Homepage URL of the tool                 |
| instructions      |           | Instructions that will be displayed at the module load in the stderr |
| require           |           | List of required module to use this tool |

## Singularity

When using a Singularity deployment, wrappers will be created for all binaries that should be made available for the tools. The list of binaries and execution options can be defined within the `meta.yml` file.

| Property         | Mandatory | Description                                                                                                   |
|------------------|:---------:|---------------------------------------------------------------------------------------------------------------|
| binaries         |     X     | List of binaries that should be wrapped                                                                       |
| binaries_paths   |           | List of folder containing binaries that should be wrapped                                                     |
| binds            |           | List of binds that should be enabled for all wrappers. Each bind must have a `src` and `dest` property.   |
| with_gpu_support |           | Enable NVidia support in Singularity for all wrappers                                                         |
| make_env_wrapper |           | Build a special wrapper called run_with_`software` to run script or software inside the container environment |

When listing binaries that should be wrapped, you can add a specific `binds` list for each binary.

It's possible to ship files with the image.def that can be copied within the Singularity image using `%files`. That can be useful for config files or extra scripts (e.g.: qdd tools)

#### If the sources aren't available publicly

For some software, for some reasons, each lab have to accept the licence agreement or are only available for academics.

For those cases, the provider usually send an url or directly the archive by email.

The packages can therefore not be available under Conda. Nor can we go through the classic Singularity image mechanism which downloads the sources directly from the provider. Each platform must accept the licenses and manually make the sources available locally on the infrastructure (Cf: `$SOFTWARE_SRC_PATH`). Thus, this type of package is optional and is platform dependent.

This gives:
 - If `$SOFTWARE_SRC_PATH` is not given by the infrastructure -> Skipped 🦘
 - If the sources are not available locally on the infrastructure -> Skipped 🦘
 - Otherwise the Singularity image will be built on compatible infrastructures.

| Property         | Mandatory | Description                                                                                                   |
|------------------|:---------:|---------------------------------------------------------------------------------------------------------------|
| singularity_url_src |     X     | URL where the sources can be downloaded |
| singularity_local_src |     X     | List of the different archive that composed the src (ex: foobar.tgz) |
| license_constraint |     X     | So far 'academic_only' or 'license_agreement' or 'commercial_software' |
| license_url |          | An URL that link the License text |

## Best practices

### Is it worth a deployment ?

Before adding a tool to the repository make that it is worth a deployment.

If the tool you want to deploy is actually only a librairy for a coding langage it might not be useful to create a standalone deployment for it.

Many langage already provided as tools like Python or R support local package installation. This mean that a given user can install missing packages in her home directory and those packages will be added to the packages already provided by the tool at runtime. For instance, if a user would like to use the `pyslim` library, she can install it manually like this :

```bash
module load python/3.9
pip install pyslim==1.0.4
```

If the library is required for a specific training or is really popular then try to add it to the existing langage environment :
- [For Python](/tools/python-pytorch-tensorflow)
- [For R](/tools/r)

### Think reproducible

Make sure the environment or apptainer image you create can be regenerated at anytime and would result in the same tool version.

Especially in an apptainer image, avoid deploying a tool using a package repository if you cannot strictly specify the version you want to deploy.

### Need GPU power ?

Many software compatible with GPU rely on Nvidia CUDA.

Apptainer is able to give access to the host CUDA driver through the `--nv` flag. This flag tells Apptainer that it must mount the host cuda driver (libcuda) inside the container at runtime.

If you deploy a software that is already compiled and doesn't required any shared cuda toolkit, don't use deploy any unnecessary cuda libraries or toolkits. Just make sure to add the `with_gpu_support` attribute in your `meta.yml` file.

See [guppy deployment](/tools/guppy/6.5.7-gpu) as an example.


## Troubleshooting

If your merge request has been opened but is not being processed, you may need to enable shared runners on your forked repository: 
under "Settings" > "CI/CD" > "Runners" > "Shared runners", check "Enable shared runners" 
