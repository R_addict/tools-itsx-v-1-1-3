#!/usr/bin/env bash

# set -x

source ci/utils.sh

umask 0022

function create_module_file {
    export software=$1
    export version=$2
    export software_alias=$3
    export version_alias=$4
    export conda_path=${CONDA_HOME}/bin/
    export env_path=${CONDA_HOME}/envs/${software}-${version}
    export env_content=$(run "ls ${env_path}")

    if [ -z $software_alias ]; then
        software_alias=$software
    fi

    if [ -z $version_alias ]; then
        version_alias=$version
    fi

    run "mkdir -p ${MODULEFILES_PATH}/${software_alias}"

    if [ "$REMOTE_MODE" = true ]; then
        module_file="/tmp/modulefile"
    else
        module_file="${MODULEFILES_PATH}/${software_alias}/${version_alias}"
    fi

    j2 ci/conda/modulefile.j2 tools/${software}/${version}/meta.yml > ${module_file}
    cat ${module_file}

    if [ "$REMOTE_MODE" = true ]; then
        scp $module_file ${CLUSTER_USER}@${CLUSTER_HOST}:${MODULEFILES_PATH}/${software_alias}/${version_alias}
        rm /tmp/modulefile
    fi

}

function r_BiocManager {
    CONDA_HOME=$1
    software=$2
    version=$3
    package=$4

    echo "Installing ${package}"
    run "source ${CONDA_HOME}/bin/activate ${CONDA_HOME}/envs/${software}-${version} && R -e \"BiocManager::install('${package}')\" 2>&1 | grep 'ERROR\|DONE'" false
} 

CONDA_BIN=$CONDA_HOME/bin/conda
MAMBA_BIN=$CONDA_HOME/bin/mamba
DEFAULT_BIN=$CONDA_BIN
CONDA_VERSION='22.9.0'
MAMBA_VERSION='0.27.0'
PYTHON_VERSION='3.9'

#### Conda config
echo -e "\033[1m Set Conda Channel Config \033[0m ⚙️"
# https://bioconda.github.io/user/install.html#set-up-channels
run "${CONDA_BIN} config --add channels defaults"
run "${CONDA_BIN} config --add channels bioconda"
run "${CONDA_BIN} config --add channels conda-forge"
# https://conda-forge.org/docs/user/tipsandtricks.html#how-to-fix-it
run "${CONDA_BIN} config --set channel_priority strict"

do_upgrade="no"
# warning update python version may update conda too ...
run "${CONDA_BIN} list | grep  '^python '" | grep "${PYTHON_VERSION}"
if [ $? -ne 0 ]
then
    do_upgrade="yes"
    echo -e "\033[1mAuto Update Python to ${PYTHON_VERSION} \033[0m \xF0\x9F\x9A\x80"
fi

run "${CONDA_BIN} --version 2>&1" | grep "${CONDA_VERSION}"
if [ $? -ne 0 ]
then
    echo -e "\033[1mAuto Update Conda to ${CONDA_VERSION} \033[0m \xF0\x9F\x9A\x80"
    do_upgrade="yes"
fi

run "ls ${MAMBA_BIN}"
if [ $? -ne 0 ]
then
    echo -e "\033[1mInstall Mamba\033[0m \xF0\x9F\x9A\x80"
    do_upgrade="yes"
else
    run "${CONDA_BIN} list | grep  '^mamba '" | grep "${MAMBA_VERSION}"
    if [ $? -ne 0 ]
    then
        echo -e "\033[1mAuto Update Mamba to ${MAMBA_VERSION} \033[0m \xF0\x9F\x9A\x80"
        do_upgrade="yes"
    fi
fi

if [ "${do_upgrade}" = "yes" ]
then
    echo -e "\033[1mRun the Update\033[0m \xF0\x9F\x9A\x80"
    run "${CONDA_BIN} install python=${PYTHON_VERSION} conda=${CONDA_VERSION} mamba=${MAMBA_VERSION} -c conda-forge --yes"
fi

run "${CONDA_BIN} --version"
run "${MAMBA_BIN} --version"
run "${CONDA_BIN} info"
tools_list=$1

exit_code=0

# creating envs
for software_line in $(cat $tools_list); do

    software=$(echo $software_line | cut -f1 -d/);
    version=$(echo $software_line | cut -f2 -d/);

    COMMON_UTILS_CMD="python3 ci/common_utils.py --software ${software} --version ${version} --function "
    aliases=$($COMMON_UTILS_CMD get_aliases)

    CONDA_BIN=$DEFAULT_BIN
    ### Mamba hook
    if [ -f tools/${software}/${version}/mamba.hook ]
    then
        CONDA_BIN=$MAMBA_BIN
    fi

    ### Remove hook
    if [ -f tools/${software}/${version}/remove.hook ]
    then
        echo -e "\033[1mRemove Hook Find for ${software} ${version}\033[0m 💣"
        echo "Removing conda environment ${software}-${version}"
        run "${CONDA_BIN} env remove --name ${software}-${version} --yes" 2> stderr

        if [ -n "${MODULEFILES_PATH}" ]
        then
            if [[ "$aliases" == 0 ]]; then
                echo "Removing modulefile ${software}/${version}"
                # run "rm -f ${MODULEFILES_PATH}/${software}/${version}"
            else
                for alias in $aliases; do
                    if [ $(echo $alias |  grep ".*/.*") ]; then
                        alias_software=$(echo $alias | cut -f 1 -d '/')
                        alias_version=$(echo $alias | cut -f 2 -d '/')
                        echo "Removing modulefile ${alias_software}/${alias_version}"
                        # run "rm -f ${MODULEFILES_PATH}/${alias_software}/${alias_version}"
                    else
                        echo "WARNING: the alias $alias doesn't match .*/.*"
                        continue
                    fi
                done
            fi

        fi
    fi

    ### Deprecated hook
    if [ -f tools/${software}/${version}/deprecated.hook ]
    then
        echo -e "\033[1m${software} ${version} marked as deprecated (no installation)\033[0m 🚫"
        continue
    fi

    if [ -f "tools/${software}/${version}/env.yml" ]
    then
        env_file="tools/${software}/${version}/env.yml"
        if [ "$REMOTE_MODE" = true ]; then
            env_file_path="/tmp/${CI_JOB_ID}-env_file.yml"
            scp ${env_file} ${CLUSTER_USER}@${CLUSTER_HOST}:$env_file_path
        else
            env_file_path=$env_file
        fi

        echo -e "\033[1mDeploying ${software} ${version}\033[0m \xF0\x9F\x9A\x80"

        echo "Check if conda environment exist for ${software}-${version}"
        run "ls -d $CONDA_HOME/envs/${software}-${version}" 2> stderr
        if [ $? -ne 0 ]
        then
            echo "Creating ${software}-${version} conda environment..."
            run "${CONDA_BIN} env create -p $CONDA_HOME/envs/${software}-${version} -f ${env_file_path}" 2> stderr
        else
            echo "Environment already exists"
            echo "Updating existing environment..."
            # https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#updating-an-environment
            run "${CONDA_BIN} env update -p $CONDA_HOME/envs/${software}-${version} -f ${env_file_path} --prune -v" 2> stderr
        fi

        if [ $? -eq 0 ]; then
            echo "Creating modulefile(s)..."
            if [[ "$aliases" == 0 ]]; then
                echo "Creating modulefile ${software}/${version}"
                create_module_file $software $version 2> /dev/null
            else
                for alias in $aliases; do
                    if [ $(echo $alias |  grep ".*/.*") ]; then
                        alias_software=$(echo $alias | cut -f 1 -d '/')
                        alias_version=$(echo $alias | cut -f 2 -d '/')
                        echo "Creating modulefile ${alias_software}/${alias_version}"
                        create_module_file ${software} ${version} ${alias_software} ${alias_version} 2> /dev/null
                    else
                        echo "WARNING: the alias $alias doesn't match .*/.*"
                        continue
                    fi
                done
            fi
        else
            echo -e "\xE2\x9D\x8C Error while working on conda environment"
            cat stderr
            exit_code=1
        fi

        if [ "$REMOTE_MODE" = true ]; then
            # Remove env_file_path
            # return 0 (true) even on error
            run "rm $env_file_path" || true
        fi
    fi

    if [ -f "tools/${software}/${version}/r.yml" ]
    then
        run "source ${CONDA_HOME}/bin/activate ${CONDA_HOME}/envs/${software}-${version} && R -e \"install.packages('devtools', repos='https://cran.biotools.fr/')\" 2>&1 | grep 'ERROR\|DONE'"
        run "source ${CONDA_HOME}/bin/activate ${CONDA_HOME}/envs/${software}-${version} && R -e \"install.packages('BiocManager', repos='https://cran.biotools.fr/')\" 2>&1 | grep 'ERROR\|DONE'"

        #run "cp ${CONDA_HOME}/envs/${software}-${version}/lib/jvm/lib/server/* ${CONDA_HOME}/envs/${software}-${version}/lib/"

        echo -e "\n\n🤞 Installing Cran R packages..."
        if [ "$(yq .cran.packages tools/${software}/${version}/r.yml | wc -l)" -gt 1 ]
        then
            for package in $(yq .cran.packages tools/${software}/${version}/r.yml --indentless-lists -y -c | cut -c 3-); do
                r_BiocManager ${CONDA_HOME} ${software} ${version} ${package}
            done
        fi

        bioconductor_version=$(yq .bioconductor.version tools/${software}/${version}/r.yml)
        if [ "$bioconductor_version" != "null" ]
        then
           run "source ${CONDA_HOME}/bin/activate ${CONDA_HOME}/envs/${software}-${version} && R -e \"BiocManager::install(version='${bioconductor_version}', ask=FALSE)\" 2>&1 | grep 'ERROR\|DONE'"
        fi

        echo -e "\n\n🤞 Installing Bioconductor R packages..."
        if [ "$(yq .bioconductor.packages tools/${software}/${version}/r.yml | wc -l)" -gt 1 ]
        then
            for package in $(yq .bioconductor.packages tools/${software}/${version}/r.yml --indentless-lists -y -c | cut -c 3-); do
                r_BiocManager ${CONDA_HOME} ${software} ${version} ${package}
            done
        fi

        echo -e "\n\n🤞 Installing GitHub R packages..."
        if [ "$(yq .github.packages tools/${software}/${version}/r.yml | wc -l)" -gt 1 ]
        then
            for package in $(yq .github.packages tools/${software}/${version}/r.yml --indentless-lists -y -c | cut -c 3-); do
                r_BiocManager ${CONDA_HOME} ${software} ${version} ${package}
            done
        fi
    fi

    if [ -f "tools/${software}/${version}/post-install.sh" ]
    then
        post_install_file="tools/${software}/${version}/post-install.sh"
        if [ "$REMOTE_MODE" = true ]; then
            post_install_script="/tmp/post-install-${software}-${version}.sh"
            scp ${post_install_file} ${CLUSTER_USER}@${CLUSTER_HOST}:${post_install_script}
        else
            post_install_script=${post_install_file}
        fi
        run "chmod +x ${post_install_script}"
        run "CONDA_HOME=${CONDA_HOME} bash -e ${post_install_script}"
        if [ $? -ne 0 ]
        then
            echo -e "\xE2\x9D\x8C Error while post install"
            exit_code=1
        fi
    fi

done

exit $exit_code
